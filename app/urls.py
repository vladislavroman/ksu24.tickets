from django.urls import include, path
from rest_framework import routers

from auth import urls as auth_urls
from tasks import urls as tasks_urls


router = routers.DefaultRouter()

urlpatterns = [
    path('', include(router.urls)),
    # path('api/', include('rest_framework.urls', namespace='rest_framework')),
    path('auth/', include((auth_urls.router.urls, 'auth'), )),
    path('tasks/', include((tasks_urls.router.urls, 'tasks'), )),
]
