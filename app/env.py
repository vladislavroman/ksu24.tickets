from pathlib import Path


BASE_DIR = Path(__file__).resolve().parent.parent

DB = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    },
    'postgres': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'ticket_db',
        'USER': 'ticket_user',
        'PASSWORD': 'ticket_pass',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    },
}

