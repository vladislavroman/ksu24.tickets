from django.db import models
from django.contrib.auth.models import User

from . import validators


class Task(models.Model):
    description = models.TextField(
        verbose_name="Опис",
        help_text="Опишіть постанову задачі",
    )
    due_date = models.DateField(
        verbose_name="Дата завершення",
    )
    assign_to = models.ManyToManyField(  # Appointees (students or workers)
        to=User,
        verbose_name="Призначений для",
    )
    appointer = models.ForeignKey(  # Worker who appointed task
        to=User,
        verbose_name="Призначаючий",
        related_name="Призначаючий",
        on_delete=models.CASCADE,
    )
    created_at = models.DateTimeField(
        verbose_name="Дата створення",
        auto_now_add=True,
    )
    modified_at = models.DateTimeField(
        verbose_name="Дата змінення",
        auto_now=True,
    )
    status = models.CharField(
        verbose_name="Статус виконання",
        max_length=20,
        choices=validators.StatusChoices,
    )
