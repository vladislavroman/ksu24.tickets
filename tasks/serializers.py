from rest_framework import serializers

from .models import (
    Task
)


class TaskSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Task
        fields = [
            'description',
            'due_date',
            'assign_to',
            'appointer',
            'created_at',
            'modified_at',
            'status',
        ]
