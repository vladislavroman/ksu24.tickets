from rest_framework import viewsets

from .models import (
    Task,
)
from .serializers import (
    TaskSerializer,
)
from .paginators import (
    StandardResultsSetPagination,
)


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all().order_by('-created_at')
    serializer_class = TaskSerializer
    permission_classes = []
    pagination_class = StandardResultsSetPagination
