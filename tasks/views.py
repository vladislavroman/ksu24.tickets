from rest_framework import generics

from .models import (
    Task,
)
from .serializers import (
    TaskSerializer,
)
from .paginators import (
    StandardResultsSetPagination,
)


class TaskListView(generics.ListCreateAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    pagination_class = StandardResultsSetPagination


class TaskDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
