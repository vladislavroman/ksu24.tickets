from django.dispatch.dispatcher import receiver
from django.db.models.signals import (
    post_save,
)
from django.core.mail import send_mail
from django.conf import settings

from .models import (
    Task,
)


@receiver(post_save, sender=Task)
def send_email_after_save(sender, instance, **kwargs):
    send_mail(
        subject="KSU24 New Task",
        message=f"You have been assigned a new task '{instance.description}'\nAssigner: {instance.appointer.firstname} {instance.appointer.lastname}\nДата завершення: {instance.due_date}",
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[user.email for user in instance.assign_to],
        fail_silently=False,
    )
